package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/todo/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pongxxxxx",
		})
	})

	r.GET("/todo/hello/:name", func(c *gin.Context) {
		name := c.Param("name")
		c.JSON(http.StatusOK, gin.H{
			"message": "Hello, " + name + "!",
		})
	})

	r.GET("/todo/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Hello, World!",
		})
	})

	r.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status": "UP",
		})
	})
	// run server on port 8080
	if err := r.Run(":9080"); err != nil {
		panic(err)
	}
}
