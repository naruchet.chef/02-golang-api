package main

import "testing"

func TestMain(t *testing.T) {
	a := 1
	b := 1

	if a != b {
		t.Errorf("a (%d) is not equal to b (%d)", a, b)
	}
}
